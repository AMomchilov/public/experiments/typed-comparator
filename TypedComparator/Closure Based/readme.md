The comparison builder implementation in this directory uses a linked list of closures (closures capturing closures which capture closures, ...) to implement a a pipeline of comparison steps.

The `TypedComparator` struct exists to wrap a comparison closure, so that you can use a fluent-like chaining style at the call site.