import Foundation

/// A comparator that takes two `Input`s, and tries to compare them
/// If there is no defined ordering, two `Downstream` values are sent
/// a downstream tie-breaker, in order to achieve a strict ordering.
struct TypedComparator<Input, Downstream> {
	private let comparator: (Input, Input) -> ComparisonDetermination<Downstream>
	
	init(_ comparator: @escaping (Input, Input) -> ComparisonDetermination<Downstream>) {
		self.comparator = comparator
	}
	
	func breakTies<NewDownstream>(
		with tieBreaker: TypedComparator<Downstream, NewDownstream>
	) -> TypedComparator<Input, NewDownstream> {
		return self.breakTies(with: tieBreaker.comparator)
	}
	
	func breakTies<NewDownstream>(
		with tieBreaker: @escaping(Downstream, Downstream) -> ComparisonDetermination<NewDownstream>
	) -> TypedComparator<Input, NewDownstream> {
		return TypedComparator<Input, NewDownstream> {
			switch self.comparator($0, $1) {
				case .decided(let result): return .decided(result)
				case .breakTies(let a, let b): return tieBreaker(a, b)
			}
		}
	}
	
	func breakTies(
		with tieBreaker: @escaping(Downstream, Downstream) -> ComparisonResult
	) -> TypedComparator<Input, Never> {
		return TypedComparator<Input, Never> {
			switch self.comparator($0, $1) {
				case .decided(let result): return .decided(result)
				case .breakTies(let a, let b): return .decided(tieBreaker(a, b))
			}
		}
	}
	
	func castDownstream<NewDownstream>(to: NewDownstream.Type)
	-> TypedComparator<Input, NewDownstream> {
		TypedComparator<Input, NewDownstream> {
			switch self.comparator($0, $1) {
				case .decided(let result): return .decided(result)
				case .breakTies(let oldA, let oldB):
					guard let a = oldA as? NewDownstream, let b = oldB as? NewDownstream else {
						fatalError("Expected \(oldA) and \(oldB) to both be of type \(NewDownstream.self)")
					}
					return .breakTies(a, b)
			}
		}
	}
}

extension TypedComparator where Input: NSObject, Downstream == Never {	
	var toFoundationComparator: Comparator {
		return { anyA, anyB in
			guard let a = anyA as? Input, let b = anyB as? Input else {
				fatalError("Expected \(anyA) and \(anyB) to both be of type \(Input.self)")
			}
			
			return self.comparator(a, b).toFoundationComparisonResult
		}
	}
}
