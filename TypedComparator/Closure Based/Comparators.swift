//
//  Comparators.swift
//  TypedComparator
//
//  Created by Alexander Momchilov on 2020-12-29.
//

import Foundation

enum Comparators {
	static let nsNullsFirst = TypedComparator<NSObject, NSObject> {
			switch ($0 is NSNull, $1 is NSNull) {
				case ( true,  true): return .orderedSame
				case ( true, false): return .orderedAscending
				case (false,  true): return .orderedDescending
				case (false, false): return .breakTies($0, $1)
			}
		}
		
	static let nsNullsLast = TypedComparator<NSObject, NSObject> {
		switch ($0 is NSNull, $1 is NSNull) {
			case ( true,  true): return .orderedSame
			case ( true, false): return .orderedDescending
			case (false,  true): return .orderedAscending
			case (false, false): return .breakTies($0, $1)
		}
	}

	static let emptyStringsFirst = TypedComparator<NSString, NSString> {
		switch ($0.length == 0, $1.length == 0) {
			case ( true,  true): return .orderedSame
			case ( true, false): return .orderedAscending
			case (false,  true): return .orderedDescending
			case (false, false): return .breakTies($0, $1)
		}
	}
}
