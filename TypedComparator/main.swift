//
//  main.swift
//  TypedComparator
//
//  Created by Alexander Momchilov on 2020-12-29.
//

import Foundation
import OSLog

let log = OSLog(
    subsystem: "ca.momchilov.TypedComparator",
    category: "main"
)

let signpostID = OSSignpostID(log: log)

Thread.sleep(forTimeInterval: 3)


let handRolledComparator: Comparator = { anyA, anyB in
    switch (anyA is NSNull, anyB is NSNull) {
        case ( true,  true): return .orderedSame
        case ( true, false): return .orderedAscending
        case (false,  true): return .orderedDescending
        case (false, false): break // Continue below to break ties
    }
    
    guard let a = anyA as? NSString, let b = anyB as? NSString else {
        fatalError("Expected \(anyA) and \(anyB) to both be of type \(NSString.self)")
    }
    
    switch (a.length == 0, b.length == 0) {
        case ( true,  true): return .orderedSame
        case ( true, false): return .orderedAscending
        case (false,  true): return .orderedDescending
        case (false, false): break  // Continue below to break ties
    }
    
    return a.localizedStandardCompare(b as String)
}


let input3: [String?] = [nil, nil, nil, nil, "", "", "", "1", "a", "A", "b", "B"].shuffled()
let array3 = input3 as NSArray
let result3 = array3.sortedArray(comparator: handRolledComparator)
print(result3)

while true {
    
    let input1: [Int?] = [nil, nil, nil, nil, 1, 2, 3, 0, -1, -2, -3].shuffled()
    let array1 = input1 as NSArray
    let input2: [String?] = [nil, nil, nil, nil, "", "", "", "1", "a", "A", "b", "B"].shuffled()
    let array2 = input2 as NSArray
    
    os_signpost(
        .begin,
        log: log,
        name: "construction",
        signpostID: signpostID
    )
    
    Thread.sleep(forTimeInterval: 0.1) ; OSMemoryBarrier()
    
    let nilsFirst = Comparators
        .nsNullsFirst
        .castDownstream(to: NSNumber.self)
        .breakTies(with: { $0.compare($1) })
        .toFoundationComparator
    
    Thread.sleep(forTimeInterval: 0.1) ; OSMemoryBarrier()
    
    let emptyAndNilsFirst = Comparators
        .nsNullsFirst
        .castDownstream(to: NSString.self)
        .breakTies(with: Comparators.emptyStringsFirst)
        .castDownstream(to: String.self)
        .breakTies(with: { $0.localizedStandardCompare($1 as String) })
        .toFoundationComparator
    
    Thread.sleep(forTimeInterval: 0.1) ; OSMemoryBarrier()

    os_signpost(
        .end,
        log: log,
        name: "construction",
        signpostID: signpostID
    )


    os_signpost(
        .begin,
        log: log,
        name: "usage",
        signpostID: signpostID
    )

    Thread.sleep(forTimeInterval: 0.1) ; OSMemoryBarrier()

    let result1 = array1.sortedArray(comparator: nilsFirst)
    let result2 = array2.sortedArray(comparator: emptyAndNilsFirst)

    Thread.sleep(forTimeInterval: 0.1) ; OSMemoryBarrier()
    
    os_signpost(
        .end,
        log: log,
        name: "usage",
        signpostID: signpostID
    )


    print(result1)
    print(result2)

    Thread.sleep(forTimeInterval: 1)
}
