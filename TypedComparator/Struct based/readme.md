The `StructBasedComparator` attempts to use value types (structs) and generics, in an attempt to eliminate all allocations, reference counting and indirection that might be involved with the closure based approach.

The hope is that if no type erasing is done, the compiler could see a massive, complex type like this:

```
StandardLocalizedStringComparison<
	NSObject,
	DownstreamAdapter<
		NSObject,
		EmptyStringsFirst<
			NSObject,
			DownstreamAdapter<
				NSObject,
				NSNullsFirst<
					NSObject,
					ComparatorStart<NSObject>
				>,
			NSString>
		>,
		String
	>
>
```

...and have enough visibility into it so as to massively inline and erase these abstractions.

It didn't really seem to work. And the coding style of writing new comparators is absolutely brutal.
