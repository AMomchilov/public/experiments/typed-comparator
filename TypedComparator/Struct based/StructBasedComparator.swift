//
//  Main Protocols.swift
//  TypedComparator
//
//  Created by Alexander Momchilov on 2020-12-29.
//

import Foundation

protocol StructBasedComparator {
    associatedtype Input // The type of object that the root of this comparator can compare
    associatedtype Downstream // The object type sent onwards for tie-breaking
    
    func compare(_: Input, _: Input) -> ComparisonDetermination<Downstream>
}

extension StructBasedComparator where Downstream == Never {
    var toFoundationComparator: Comparator {
        return { anyA, anyB in
            guard let a = anyA as? Input, let b = anyB as? Input else {
                fatalError("Expected \(anyA) and \(anyB) to both be of type \(Input.self)")
            }
            
            return self.compare(a, b).toFoundationComparisonResult
        }
    }
}
