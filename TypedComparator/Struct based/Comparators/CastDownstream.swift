//
//  CastDownstream.swift
//  TypedComparator
//
//  Created by Alexander Momchilov on 2020-12-29.
//

import Foundation

extension StructBasedComparator {
    func castingDownstream<NewDownstream>(to: NewDownstream.Type)
    -> DownstreamAdapter<Self.Input, Self, NewDownstream> {
        DownstreamAdapter(upstream: self)
    }
}

struct DownstreamAdapter<
		Input,
		Upstream: StructBasedComparator,
		Downstream
	>: StructBasedComparator where Upstream.Input == Input {
	
    private var upstream: Upstream
    
    init(upstream: Upstream) {
        self.upstream = upstream
    }
    
    func compare(_ a: Input, _ b: Input) -> ComparisonDetermination<Downstream> {
        switch upstream.compare(a, b) {
        case .breakTies(let oldA, let oldB):
            guard let a = oldA as? Downstream, let b = oldB as? Downstream else {
                fatalError("Expected \(oldA) and \(oldB) to both be of type \(Downstream.self)")
            }
            return .breakTies(a, b)
            
        case let .decided(result):
            return .decided(result)
        }
    }
}
