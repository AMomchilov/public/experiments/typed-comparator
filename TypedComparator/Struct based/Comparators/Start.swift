//
//  Start.swift
//  TypedComparator
//
//  Created by Alexander Momchilov on 2020-12-29.
//

import Foundation

/// Acts as a dummy upstream, which starts off a pipe, by just immediately forwarding the comparison down to the next comparator.
struct ComparatorStart<Input>: StructBasedComparator {
    typealias Downstream = Input
    
    func compare(_ a: Input, _ b: Input) -> ComparisonDetermination<Downstream> {
        .breakTies(a, b)
    }
}
