//
//  EmptyStringsFirst.swift
//  TypedComparator
//
//  Created by Alexander Momchilov on 2020-12-29.
//

import Foundation

extension StructBasedComparator where Self.Downstream == NSString {
    func emptyStringsFirst() -> EmptyStringsFirst<Self.Input, Self> {
        EmptyStringsFirst(upstream: self)
    }
}

struct EmptyStringsFirst<Input, Upstream: StructBasedComparator>: StructBasedComparator
	where Upstream.Input == Input,
		  Upstream.Downstream == NSString {
	
    private var upstream: Upstream
    
    init(upstream: Upstream) {
        self.upstream = upstream
    }
    
    func compare(_ a: Input, _ b: Input) -> ComparisonDetermination<NSString> {
        switch upstream.compare(a, b) {
        case .breakTies(let a, let b):
            switch (a.length == 0, b.length == 0) {
                case ( true,  true): return .orderedSame
                case ( true, false): return .orderedAscending
                case (false,  true): return .orderedDescending
                case (false, false): return .breakTies(a, b)
            }
        case let result: return result
        }
    }
}
