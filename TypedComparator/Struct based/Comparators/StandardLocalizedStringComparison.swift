//
//  StandardLocalizedStringComparison.swift
//  TypedComparator
//
//  Created by Alexander Momchilov on 2020-12-29.
//

import Foundation

extension StructBasedComparator where Downstream == String {
    func standardLocalizedStringComparison() -> StandardLocalizedStringComparison<Input, Self> {
        return StandardLocalizedStringComparison(upstream: self)
    }
}

struct StandardLocalizedStringComparison<Input, Upstream: StructBasedComparator>: StructBasedComparator
	where Upstream.Input == Input,
		  Upstream.Downstream == String {
	
    private var upstream: Upstream
    
    init(upstream: Upstream) {
        self.upstream = upstream
    }
    
    func compare(_ a: Input, _ b: Input) -> ComparisonDetermination<Never> {
        switch upstream.compare(a, b) {
        case .breakTies(let a, let b):
             return .decided(a.localizedStandardCompare(b))
        case .decided(let result): return .decided(result)
        }
    }
}
