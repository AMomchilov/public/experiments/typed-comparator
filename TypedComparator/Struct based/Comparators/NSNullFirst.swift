//
//  NSNullFirst.swift
//  TypedComparator
//
//  Created by Alexander Momchilov on 2020-12-29.
//

import Foundation

extension StructBasedComparator where Self.Downstream == NSObject {
    func nsNullsFirst() -> NSNullsFirst<Self.Input, Self> {
        NSNullsFirst(upstream: self)
    }
}

struct NSNullsFirst<Input, Upstream: StructBasedComparator>: StructBasedComparator
	where Upstream.Input == Input,
		  Upstream.Downstream == NSObject {
	
    private var upstream: Upstream
    
    init(upstream: Upstream) {
        self.upstream = upstream
    }
    
    func compare(_ a: Input, _ b: Input) -> ComparisonDetermination<NSObject> {
        switch upstream.compare(a, b) {
        case .breakTies(let a, let b):
            switch (a is NSNull, b is NSNull) {
                case ( true,  true): return .orderedSame
                case ( true, false): return .orderedAscending
                case (false,  true): return .orderedDescending
                case (false, false): return .breakTies(a, b)
            }
        case let result: return result
        }
    }
}
