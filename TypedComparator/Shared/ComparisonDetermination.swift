//
//  ComparisonDetermination.swift
//  TypedComparator
//
//  Created by Alexander Momchilov on 2020-12-29.
//

import Foundation

/// Similar to `Foundation.ComparisonResultresult`, but can model the
/// the case when no determination is made yet, because a comparison
/// needs further downstream tie-breaking.
enum ComparisonDetermination<Downstream> {
	case decided(ComparisonResult)
	case breakTies(Downstream, Downstream)
	
	static var orderedSame: ComparisonDetermination { .decided(.orderedSame) }
	static var orderedAscending: ComparisonDetermination { .decided(.orderedAscending) }
	static var orderedDescending: ComparisonDetermination { .decided(.orderedDescending) }
}

extension ComparisonDetermination where Downstream == Never {
	var toFoundationComparisonResult: ComparisonResult {
		switch self {
			case .decided(let result): return result
		}
	}
}
