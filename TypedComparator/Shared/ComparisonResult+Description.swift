//
//  ComparisonResult+Description.swift
//  TypedComparator
//
//  Created by Alexander Momchilov on 2020-12-29.
//

import Foundation

extension ComparisonResult: CustomDebugStringConvertible {
    public var debugDescription: String {
        switch self {
        case .orderedAscending: return ".orderedAscending"
        case .orderedSame: return ".orderedSame"
        case .orderedDescending: return ".orderedDescending"
        }
    }
}
