//
//  PerfTests.swift
//  PerfTests
//
//  Created by Alexander Momchilov on 2020-12-29.
//

import XCTest

class PerfTests: XCTestCase {
    override func setUpWithError() throws {
        assertionFailure("optimizations aren't turned on!")
    }
    
    func testHandRolledComparatorPerformance() {
        let handRolledComparator: Comparator = { anyA, anyB in
            switch (anyA is NSNull, anyB is NSNull) {
                case ( true,  true): return .orderedSame
                case ( true, false): return .orderedAscending
                case (false,  true): return .orderedDescending
                case (false, false): break
            }
            
            guard let a = anyA as? NSString, let b = anyB as? NSString else {
                fatalError("Expected \(anyA) and \(anyB) to both be of type \(NSString.self)")
            }
            
            switch (a.length == 0, b.length == 0) {
                case ( true,  true): return .orderedSame
                case ( true, false): return .orderedAscending
                case (false,  true): return .orderedDescending
                case (false, false): break
            }
            
            return a.localizedStandardCompare(b as String)
        }
        
        let options = XCTMeasureOptions()
        options.iterationCount = 10
        options.invocationOptions = [.manuallyStart, .manuallyStop]
        
        measure(options: options) {
            let input = randomStringArray()
            
            startMeasuring()
            input.sortedArray(comparator: handRolledComparator)
            stopMeasuring()
        }
    }
    
    func testTypedComparatorPerformance() throws {
        let emptyAndNilsFirst = Comparators
            .nsNullsFirst
            .castDownstream(to: NSString.self)
            .breakTies(with: Comparators.emptyStringsFirst)
            .castDownstream(to: String.self)
            .breakTies(with: { $0.localizedStandardCompare($1 as String) })
            .toFoundationComparator
        
        let options = XCTMeasureOptions()
        options.iterationCount = 10
        options.invocationOptions = [.manuallyStart, .manuallyStop]
        
        measure(options: options) {
            let input = randomStringArray()
            
            startMeasuring()
            input.sortedArray(comparator: emptyAndNilsFirst)
            stopMeasuring()
        }
    }
    
    func testStructBasedApproachPerformance() throws {
        let structBasedApproach = ComparatorStart<NSObject>()
            .nsNullsFirst()
            .castingDownstream(to: NSString.self)
            .emptyStringsFirst()
            .castingDownstream(to: String.self)
            .standardLocalizedStringComparison()
            .toFoundationComparator
        
        let options = XCTMeasureOptions()
        options.iterationCount = 10
        options.invocationOptions = [.manuallyStart, .manuallyStop]
        
        measure(options: options) {
            let input = randomStringArray()
            
            startMeasuring()
            input.sortedArray(comparator: structBasedApproach)
            stopMeasuring()
        }
        
    }
    
    private func randomStringArray() -> NSArray {
        randomStringArray(strings: 6_000, blanks: 1_000, nils: 3_000)
    }
    
    private func randomStringArray(strings: Int, blanks: Int, nils: Int) -> NSArray {
        let unshuffled: [String?] =
            (0..<strings).map { _ in randomString(rangeOfLengths: 2...20) } +
            (0..<blanks).map { _ in "" } +
            (0..<nils).map { _ in nil }
        
        return unshuffled.shuffled() as NSArray
    }
    
    private func randomString(rangeOfLengths: ClosedRange<Int>) -> String {
        return randomString(length: rangeOfLengths.randomElement()!)
    }
    
    private func randomString(length: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0..<length).map{ _ in letters.randomElement()! })
    }
}
